#!/bin/bash
# Hard stop on any errors
set -e

# Settings
# These shouldn't have a trailing slash
destinationprefix=/srv/archives/ftp
uploadprefix=/srv/uploads/incoming

# Validate that we are receiving the correct number of parameters
if  [ $# -ne 3 ]; then
    echo "give destination shasum tarballname";
    exit;
fi

# Load our parameters
destination=$1
hashsum=$2
fileToRelease=$3

# Ensure the hash we have collected is in a standard format
hashsum=`echo "$hashsum" | awk '{print tolower($0)}'`

### Make sure the file being released exists
fileToReleasePath="$uploadprefix/$fileToRelease"
if [ ! -f "$fileToReleasePath" ]; then
    echo "File '$fileToReleasePath' does not exist"
    exit 1
fi

### Ensure the hash given matches
# Determine what type of hash we have
hashlength=`expr length "$hashsum"`
if [ $hashlength == 128 ]; then
    filehash=`sha512sum "$fileToReleasePath"`
elif [ $hashlength == 64 ]; then
    filehash=`sha256sum "$fileToReleasePath"`
elif [ $hashlength == 40 ]; then
    echo "Weak hash SHA-1 detected - not proceeding"
    exit 2
elif [ $hashlength == 32 ]; then
    echo "Extremely weak hash MD5 detected - not proceeding"
    exit 2
else
    echo "Hash sum type not known"
    exit 2
fi

# Verify the hash
filehash=${filehash%  $fileToReleasePath}
if [ "$filehash" != "$hashsum" ]; then
    echo "Hash sum '$filehash' for file '$fileToReleasePath' does not match provided '$hashsum'"
    exit 3
fi

### Next we validate the GPG signature supplied alongside the file
### This validation isn't required for exe/dmg/appimage so if the signature does not exist we will just warn so the later confirmation can be canceled
if [ -f "$fileToReleasePath.sig" ]; then
    # Invoke GPG to validate....
    gpgv --keyring ~/release-keyring/keyring.kbx "$fileToReleasePath.sig" "$fileToReleasePath"

    # Did it pass?
    if [[ $? -ne 0 ]]; then
        echo "GPG signature validation failed - refusing to continue"
        exit 3
    fi
    # Clean line to keep the terminal tidy
    echo ""
else
    echo "WARNING: Releasing this file without a GPG signature validation."
fi

### Now we check the destination
# Remove any starting and ending slashes
destination=${destination#/}
destination=${destination%/}

# Make sure it is headed to either stable or unstable....
if [[ ! "$destination" =~ (stable|unstable)/ ]]; then
    echo "Destination is not in stable or unstable!"
    exit 4
fi
# Set the final destination
url="https://download.kde.org/$destination/$fileToRelease.mirrorlist"
destination="$destinationprefix/$destination/"

echo "Will be moving to '$destination'"
read -p "Proceed? [y/N] " proceed

if [ "$proceed" != "y" ]; then
        echo "Quitting..."
        exit 0
fi

# Save the filename for later
filename=`basename "$fileToReleasePath"`

# Create the destination
echo $destination | awk -F'/' '{ for (i=1; i<=NF; i++) {print $i}}' |
while read fname
do
    fname_complete+="$fname/"
    if [ ! -d "$fname_complete" ]; then
        mkdir $fname_complete
    fi
done

# Move the file to the final tree
mv "$fileToReleasePath" "$destination"
# Set permissions and user group properly
chmod 644 "$destination/$filename"
chgrp packager "$destination/$filename"

# Move the GPG signature as well
if [ -f "$fileToReleasePath.sig" ]; then
    # Move the file to the final tree
    mv "$fileToReleasePath.sig" "$destination"
    # Set permissions and user group properly
    chmod 644 "$destination/$filename.sig"
    chgrp packager "$destination/$filename.sig"
fi

echo ""
echo "You can spread $url as soon as it shows some mirrors."
